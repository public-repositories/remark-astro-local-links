import { visit } from 'unist-util-visit';

export function remarkAstroLocalLinks({base}) {

	const transformer = (tree) => {

		visit(tree, 'link', (link) => {

			if (link.url.startsWith("/")) {

				link.url = [
					base.replace(/\/+$/, ""),
					link.url.replace(/^\/+/, "")
				].join("/");

			}

		});
	};

	return function attacher() {
		return transformer;
	};
}
	
