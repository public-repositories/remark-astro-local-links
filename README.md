# Why

[Astro](astro.build/) is a website-building framework with excellent support for markdown files right out-of-the-box. However, when the website is deployed to a non-root base path (using the [base](https://docs.astro.build/en/reference/configuration-reference/#base) parameter), the links in the rendered markdown are not automatically updated to the new base path. This [remark](https://remark.js.org/) plugin solves this issue by finding and manipulating local links in the [mdast](https://github.com/syntax-tree/mdast) tree.

# Usage

To use this plugin, import the `remarkAstroLocalLinks` function, and then include it in the list of remark plugins in your Astro config. Make sure that you pass the base path to it as an argument!

Here is a minimal Astro config (typically, the file `astro.config.mjs`) which should work for most users: 
```js 
import { remarkAstroLocalLinks } from 'remark-astro-local-links';

const basePath = "/new-base-path";

export default defineConfig({

  site: "https://www.mywebsite.xyz",

  base: basePath,

  markdown: {
    remarkPlugins: [
		remarkAstroLocalLinks({base: basePath})
	]
  }

});
```

# Contributing

I think it is a very simple plugin and there's not much else to do here. However, if you find an error, or you have ideas to make the plugin more robust (it has only been tested on a Debian 11 box), feel free to send in a merge request. 

Finally a big thanks to the Astro community, especially [@hippotastic](https://github.com/hippotastic), for the pointers!

